//REQUIRED CODE FOR ALL JQUERY VALIDATION--------------------------------------------------------------------------------------
$('.mform').validate({
    debug: true,
    submitHandler: function(form) {
         form.submit();
   }
});
$.validator.addMethod(
        "regex",
        function(value, element, regexp) {
            var re = new RegExp(regexp);
            return this.optional(element) || re.test(value);
        },
        "Please check your input."
);
$("#id_submitbutton_cancel").addClass("cancel");
//END REQUIRED CODE FOR ALL JQUERY VALIDATION--------------------------------------------------------------------------------------

//EXAMPLE VALIDATIONS-----------------------------------------------------------
// For these examples, Q1 is the field name.
// In the view html, you put the field in a div with class name =qc_Q1: <div class="qc_Q1">[[Q1]]</div>




jQuery.validator.addMethod('dobleValor', function(value, element) {
        return parseFloat(value) <= 2*parseFloat($(".validation_x_value").val()) && parseFloat(value) >= 0;
    }, 'ALERT');

$( ".qc_Q1 input" ).each(function(){
           $(this).rules('add', {
          range: [0, 2],
          required: true,
          messages: {
               range: "Data out of range. Please check the units on the data entered."
          }
  });
});
	
	
	
$( ".qc_Q2 input" ).each(function(){
           $(this).rules('add', {
          range: [1, 75],
          required: true,
          messages: {
               range: "Data out of range. Please check the units on the data entered."
          }
  });
});

$( ".qc_Q6 input" ).each(function(){
           $(this).rules('add', {
          range: [0, 2],
          required: true,
          messages: {
               range: "Data out of range. Please check the units on the data entered."
          }
  });
});

$( ".qc_Q7 input" ).each(function(){
           $(this).rules('add', {
          range: [1, 75],
          required: true,
          messages: {
               range: "Data out of range. Please check the units on the data entered."
          }
  });
});


$( "input[class='validation_x_value']" ).each(function(){
           $(this).rules('add', {
          required: true,
          number: true,
          messages: {
               range: "This field must be a number"
          }
  });
});

$( "input[class^='validation_only_range_function(between_0_and_no_more_than_twice_the_value_of_x)']" ).each(function(){
           $(this).rules('add', {
          required: true,
          dobleValor: true,
         
  });
});

//EXACT NUMBER OF DECIMAL PLACES 
 $( ".qc_Q2 input" ).each(function(){         
    $(this).rules('add', {   
      required: true,  
      regex:  /^[+-]?\d{0,}\.\d{2}$/,      
       messages: {              
      regex: "The number must be 2 decimal places"      
     }  
   });
 });


 //END EXACT NUMBER OF DECIMAL PLACES


