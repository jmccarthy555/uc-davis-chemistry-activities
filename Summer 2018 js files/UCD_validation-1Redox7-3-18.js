$('.mform').validate({
    debug: false,
    submitHandler: function(form) {
         // Remove navigation prompt.
         window.onbeforeunload = null;
         form.submit();
   }
});
$.validator.addMethod(
        "regex",
        function(value, element, regexp) {
            var re = new RegExp(regexp);
            return this.optional(element) || re.test(value);
        },
        "Please check your input."
);

jQuery.validator.addMethod('dobleValor', function(value, element) {
        return parseFloat(value) <= 2*parseFloat($(".validation_x_value").val()) && parseFloat(value) >= 0;
    }, 'ALERT');

$( ".qc_validation_only_number_with_decimals input" ).each(function(){
           $(this).rules('add', {
          required: true,
          number: true,
          messages: {
               range: "This field must be a number"
          }
  });
});


	
$( ".qc_Q1 input" ).each(function(){
           $(this).rules('add', {
          range: [.3, .71],
          required: true,
          messages: {
               range: "Data out of range. Please check the units on the data entered."
          }
  });
});

//Q3 EXACT NUMBER OF DECIMAL PLACES (2)
 $( ".qc_Q3T1 input" ).each(function(){         
    $(this).rules('add', {   
      required: true,  
      regex:  /^[+-]?\d{0,}\.\d{2}$/,      
       messages: {              
      regex: "The number must be 2 decimal places"      
     }  
   });
 });
 
  $( ".qc_Q3T2 input" ).each(function(){         
    $(this).rules('add', {   
      required: true,  
      regex:  /^[+-]?\d{0,}\.\d{2}$/,      
       messages: {              
      regex: "The number must be 2 decimal places"      
     }  
   });
 });

 $( ".qc_Q3T3 input" ).each(function(){         
    $(this).rules('add', {   
      required: true,  
      regex:  /^[+-]?\d{0,}\.\d{2}$/,      
       messages: {              
      regex: "The number must be 2 decimal places"      
     }  
   });
 });
 
//END Q4

//Q4 EXACT NUMBER OF DECIMAL PLACES (2)
 $( ".qc_Q4T1 input" ).each(function(){         
    $(this).rules('add', {   
      required: true,  
      regex:  /^[+-]?\d{0,}\.\d{2}$/,      
       messages: {              
      regex: "The number must be 2 decimal places"      
     }  
   });
 });
 
  $( ".qc_Q4T2 input" ).each(function(){         
    $(this).rules('add', {   
      required: true,  
      regex:  /^[+-]?\d{0,}\.\d{2}$/,      
       messages: {              
      regex: "The number must be 2 decimal places"      
     }  
   });
 });

 $( ".qc_Q4T3 input" ).each(function(){         
    $(this).rules('add', {   
      required: true,  
      regex:  /^[+-]?\d{0,}\.\d{2}$/,      
       messages: {              
      regex: "The number must be 2 decimal places"      
     }  
   });
 });
 
//END Q4


$( ".qc_Q3T1 input" ).each(function(){
//$( "input[class='Q3T1']" ).each(function(){
           $(this).rules('add', {
          range: [1, 30],
          required: true,
          messages: {
               range: "Data out of range. Please check the units on the data entered."
          }
  });
});

$( ".qc_Q3T2 input" ).each(function(){
//$( "input[class='Q3T2']" ).each(function(){
           $(this).rules('add', {
          range: [1, 30],
          required: true,
          messages: {
               range: "Data out of range. Please check the units on the data entered."
          }
  });
});

$( ".qc_Q3T3 input" ).each(function(){
//$( "input[class='Q3T3']" ).each(function(){
           $(this).rules('add', {
          range: [1, 30],
          required: true,
          messages: {
               range: "Data out of range. Please check the units on the data entered."
          }
  });
});
$( ".qc_Q4T1 input" ).each(function(){
//$( "input[class='Q4T1']" ).each(function(){
           $(this).rules('add', {
          range: [1, 75],
          required: true,
          messages: {
               range: "Data out of range. Please check the units on the data entered."
          }
  });
});

$( ".qc_Q4T2 input" ).each(function(){
//$( "input[class='Q4T2']" ).each(function(){
           $(this).rules('add', {
          range: [1, 75],
          required: true,
          messages: {
               range: "Data out of range. Please check the units on the data entered."
          }
  });
});

$( ".qc_Q4T3 input" ).each(function(){
//$( "input[class='Q4T3']" ).each(function(){
           $(this).rules('add', {
          range: [1, 75],
          required: true,
          messages: {
               range: "Data out of range. Please check the units on the data entered."
          }
  });
});

// $( ".qc_Q7 input" ).each(function(){         
//    $(this).rules('add', {   
//      required: true,  
//      regex:  /^[+-]?\d{0,}\.\d{3}$/,      
//       messages: {              
//      regex: "The number must be 3 decimal places"      
//     }  
//   });
// });
 


$( ".qc_Q12T1 input" ).each(function(){
//$( "input[class='Q12T1']" ).each(function(){
           $(this).rules('add', {
          range: [.1, 2],
          required: true,
          messages: {
               range: "Data out of range. Please check the units on the data entered."
          }
  });
});

$( ".qc_Q12T2 input" ).each(function(){
//$( "input[class='Q12T2']" ).each(function(){
           $(this).rules('add', {
          range: [.1, 2],
          required: true,
          messages: {
               range: "Data out of range. Please check the units on the data entered."
          }
  });
});

$( ".qc_Q12T3 input" ).each(function(){
//$( "input[class='Q12T3']" ).each(function(){
           $(this).rules('add', {
          range: [.1, 2],
          required: true,
          messages: {
               range: "Data out of range. Please check the units on the data entered."
          }
  });
});


$( ".qc_Q13T1 input" ).each(function(){
//$( "input[class='Q13T1']" ).each(function(){
           $(this).rules('add', {
          range: [1, 75],
          required: true,
          messages: {
               range: "Data out of range. Please check the units on the data entered."
          }
  });
});

$( ".qc_Q13T2 input" ).each(function(){
//$( "input[class='Q13T2']" ).each(function(){
           $(this).rules('add', {
          range: [1, 75],
          required: true,
          messages: {
               range: "Data out of range. Please check the units on the data entered."
          }
  });
});

$( ".qc_Q13T3 input" ).each(function(){
//$( "input[class='Q13T3']" ).each(function(){
           $(this).rules('add', {
          range: [1, 75],
          required: true,
          messages: {
               range: "Data out of range. Please check the units on the data entered."
          }
  });
});






$( "input[class='validation_x_value']" ).each(function(){
           $(this).rules('add', {
          required: true,
          number: true,
          messages: {
               range: "This field must be a number"
          }
  });
});

$( "input[class^='validation_only_range_function(between_0_and_no_more_than_twice_the_value_of_x)']" ).each(function(){
           $(this).rules('add', {
          required: true,
          dobleValor: true,
         
  });
});

$("#id_submitbutton_cancel").addClass("cancel");



