//REQUIRED CODE FOR ALL JQUERY VALIDATION--------------------------------------------------------------------------------------
$('.mform').validate({
    debug: true,
    submitHandler: function(form) {
         form.submit();
   }
});
$.validator.addMethod(
        "regex",
        function(value, element, regexp) {
            var re = new RegExp(regexp);
            return this.optional(element) || re.test(value);
        },
        "Please check your input."
);
$("#id_submitbutton_cancel").addClass("cancel");
//END REQUIRED CODE FOR ALL JQUERY VALIDATION--------------------------------------------------------------------------------------


jQuery.validator.addMethod('dobleValor', function(value, element) {
        return parseFloat(value) <= 2*parseFloat($(".validation_x_value").val()) && parseFloat(value) >= 0;
    }, 'ALERT');

	
	
	
$( ".qc_Q3 input" ).each(function(){
           $(this).rules('add', {
          range: [.05, 2.0],
          required: true,
          messages: {
               range: "Data out of range. Please check the units on the data entered."
          }
  });
});

$( ".qc_Q7 input" ).each(function(){
           $(this).rules('add', {
          range: [.338, .342],
          required: true,
          messages: {
               range: "Data out of range. Please check the units on the data entered."
          }
  });
});


$( ".qc_Q10 input" ).each(function(){
           $(this).rules('add', {
          range: [.05, 2.0],
          required: true,
          messages: {
               range: "Data out of range. Please check the units on the data entered."
          }
  });
});

$( ".qc_Q16 input" ).each(function(){
           $(this).rules('add', {
          range: [.05, 2.0],
          required: true,
          messages: {
               range: "Data out of range. Please check the units on the data entered."
          }
  });
});

$( ".qc_Q30 input" ).each(function(){
           $(this).rules('add', {
          range: [.005, 2.0],
          required: true,
          messages: {
               range: "Data out of range. Please check the units on the data entered."
          }
  });
});


//SCIENTIFIC NOTATION (as of 6-19-18) revised to include both e and E  ----------------------------
function scientific_notation33(){
var aux = $( ".qc_Q33 input" ).val();
var a = "";
  for(i=0; i<=aux.length; i++) { 
      a = a + aux.charAt(i);
    }
      if(a.includes("E") || a.includes("e") && a.includes(".") && a.charAt(a.length-1)!="e" && a.charAt(a.length-1)!="-" && a.charAt(a.length-1)!="+" 
        && jQuery.isNumeric( $( ".qc_Q33 input" ).val() )){
        
        $( ".qc_Q33 input" ).each(function(){         
        $(this).rules('add', {   
        required: true,  
        regex:  /((\b[0-9]+)?\.)?\b[0-9]+([eE][-+]?[0-9]+)?\b/,      
        messages: {              
        regex: "The number must be a scientific notation. Example: -2.5e4 or 47e-3"      
        }  
      });
    });
    }else{
      $( ".qc_Q33 input" ).each(function(){         
        $(this).rules('add', {   
        required: true,  
        regex:  /^[0-9]^[.][0-9]^[eE]/,
        messages: {              
        regex: "You must express the number in scientific notation."      
        }  
      });
    });
    }
}
$( ".qc_Q33 input" ).attr('onkeyup','scientific_notation33()');

function scientific_notation35(){
var aux = $( ".qc_Q35 input" ).val();
var a = "";
  for(i=0; i<=aux.length; i++) { 
      a = a + aux.charAt(i);
    }
      if(a.includes("E") || a.includes("e") && a.includes(".") && a.charAt(a.length-1)!="e" && a.charAt(a.length-1)!="-" && a.charAt(a.length-1)!="+" 
        && jQuery.isNumeric( $( ".qc_Q35 input" ).val() )){
        
        $( ".qc_Q35 input" ).each(function(){         
        $(this).rules('add', {   
        required: true,  
        regex:  /((\b[0-9]+)?\.)?\b[0-9]+([e][-+]?[0-9]+)?\b/,      
        messages: {              
        regex: "The number must be a scientific notation. Example: -2.5e4 or 47e-3"      
        }  
      });
    });
    }else{
      $( ".qc_Q35 input" ).each(function(){         
        $(this).rules('add', {   
        required: true,  
        regex:  /^[0-9]^[.][0-9]^[e]/,
        messages: {              
        regex: "You must express the number in scientific notation."      
        }  
      });
    });
    }
}
$( ".qc_Q35 input" ).attr('onkeyup','scientific_notation35()');
//END SCIENTIFIC NOTATION (as of 6-19-18)----------------------------

/* //SCIENTIFIC NOTATION-old
 $( ".qc_Q33 input" ).each(function(){         
    $(this).rules('add', {   
      required: true,  
      regex:  /^[+-]?[.0-9]+[.,]?[0-9]+?[eE]?[+-]?[0-9]+?$/,        
       messages: {              
      regex: "The number must be a scientific notation. Example: -2.5e4 or 47e-3"      
     }  
   });
 });

$( ".qc_Q35 input" ).each(function(){         
    $(this).rules('add', {   
      required: true,  
      regex:  /^[+-]?[.0-9]+[.,]?[0-9]+?[eE]?[+-]?[0-9]+?$/,      
       messages: {              
      regex: "The number must be a scientific notation. Example: -2.5e4 or 47e-3"      
     }  
   });
 });
//END SCIENTIFIC NOTATION-old */