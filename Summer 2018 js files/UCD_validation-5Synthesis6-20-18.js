//REQUIRED CODE FOR ALL JQUERY VALIDATION--------------------------------------------------------------------------------------
$('.mform').validate({
    debug: true,
    submitHandler: function(form) {
         form.submit();
   }
});
$.validator.addMethod(
        "regex",
        function(value, element, regexp) {
            var re = new RegExp(regexp);
            return this.optional(element) || re.test(value);
        },
        "Please check your input."
);
$("#id_submitbutton_cancel").addClass("cancel");
//END REQUIRED CODE FOR ALL JQUERY VALIDATION--------------------------------------------------------------------------------------

//EXAMPLE VALIDATIONS-----------------------------------------------------------
// For these examples, Q1 is the field name.
// In the view html, you put the field in a div with class name =qc_Q1: <div class="qc_Q1">[[Q1]]</div>

//RANGE 

jQuery.validator.addMethod('dobleValor', function(value, element) {
        return parseFloat(value) <= 2*parseFloat($(".validation_x_value").val()) && parseFloat(value) >= 0;
    }, 'ALERT');

	
	
	
$( ".qc_Q6 input" ).each(function(){
           $(this).rules('add', {
          range: [6, 10],
          required: true,
          messages: {
               range: "Data out of range. Please check the units on the data entered."
          }
  });
});

$( ".qc_Q10 input" ).each(function(){
           $(this).rules('add', {
          range: [.001, 4],
          required: true,
          messages: {
               range: "Data out of range. Please check the units on the data entered."
          }
  });
});

//EXACT NUMBER OF DECIMAL PLACES
 $( ".qc_Q6 input" ).each(function(){         
    $(this).rules('add', {   
      required: true,  
      regex:  /^[+-]?\d{0,}\.\d{2}$/,      
       messages: {              
      regex: "The number must be 2 decimal places"      
     }  
   });
 });

 $( ".qc_Q10 input" ).each(function(){         
    $(this).rules('add', {   
      required: true,  
      regex:  /^[+-]?\d{0,}\.\d{3}$/,      
       messages: {              
      regex: "The number must be 3 decimal places"      
     }  
   });
 });

 $( ".qc_Q11 input" ).each(function(){         
    $(this).rules('add', {   
      required: true,  
      regex:  /^[+-]?\d{0,}\.\d{2}$/,      
       messages: {              
      regex: "The number must be 2 decimal places"      
     }  
   });
 });

 //END EXACT NUMBER OF DECIMAL PLACES


$( "input[class='validation_x_value']" ).each(function(){
           $(this).rules('add', {
          required: true,
          number: true,
          messages: {
               range: "This field must be a number"
          }
  });
});

$( "input[class^='validation_only_range_function(between_0_and_no_more_than_twice_the_value_of_x)']" ).each(function(){
           $(this).rules('add', {
          required: true,
          dobleValor: true,
         
  });
});




