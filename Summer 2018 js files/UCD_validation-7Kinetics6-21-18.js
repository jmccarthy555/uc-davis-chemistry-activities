//REQUIRED CODE FOR ALL JQUERY VALIDATION--------------------------------------------------------------------------------------
$('.mform').validate({
    debug: true,
    submitHandler: function(form) {
         form.submit();
   }
});
$.validator.addMethod(
        "regex",
        function(value, element, regexp) {
            var re = new RegExp(regexp);
            return this.optional(element) || re.test(value);
        },
        "Please check your input."
);
$("#id_submitbutton_cancel").addClass("cancel");
//END REQUIRED CODE FOR ALL JQUERY VALIDATION--------------------------------------------------------------------------------------

//EXAMPLE VALIDATIONS-----------------------------------------------------------
// For these examples, Q1 is the field name.
// In the view html, you put the field in a div with class name =qc_Q1: <div class="qc_Q1">[[Q1]]</div>

//RANGE 

	
	
	
$( ".qc_Q4 input" ).each(function(){
           $(this).rules('add', {
          range: [.49, 1.51],
          required: true,
          messages: {
               range: "The ratio you entered is outside the range expected for this experiment. The ratio should be fairly close to 1 taking into account experimental error. Please consider your value to be nearly 1 when answering the following question.  "
          }
  });
});



//Custom methods for Q7-Q10
//Q7---------------------------------
jQuery.validator.addMethod('rangecheckQ71', function(value, element) {
        return parseFloat(value) <= 1.05*(1/(45+273.2)) && parseFloat(value) >= .95*(1/(45+273.2));
    }, 'Data out of range. Please check the units on the data entered.');


$( ".qc_Q7-1 input" ).each(function(){ 
           $(this).rules('add', {
          required: true,
          rangecheckQ71: true,
         
  });
});

jQuery.validator.addMethod('rangecheckQ72', function(value, element) {
        return parseFloat(value) <= 1.05*(1/(55+273.2)) && parseFloat(value) >= .95*(1/(55+273.2));
    }, 'Data out of range. Please check the units on the data entered.');


$( ".qc_Q7-2 input" ).each(function(){ 
           $(this).rules('add', {
          required: true,
          rangecheckQ72: true,
         
  });
});

jQuery.validator.addMethod('rangecheckQ73', function(value, element) {
        return parseFloat(value) <= 1.05*(1/(65+273.2)) && parseFloat(value) >= .95*(1/(65+273.2));
    }, 'Data out of range. Please check the units on the data entered.');


$( ".qc_Q7-3 input" ).each(function(){ 
           $(this).rules('add', {
          required: true,
          rangecheckQ73: true,
         
  });
});

jQuery.validator.addMethod('rangecheckQ74', function(value, element) {
        return parseFloat(value) <= 1.05*(1/(75+273.2)) && parseFloat(value) >= .95*(1/(75+273.2));
    }, 'Data out of range. Please check the units on the data entered.');


$( ".qc_Q7-4 input" ).each(function(){ 
           $(this).rules('add', {
          required: true,
          rangecheckQ74: true,
         
  });
});

jQuery.validator.addMethod('rangecheckQ75', function(value, element) {
        return parseFloat(value) <= 1.05*(1/(80+273.2)) && parseFloat(value) >= .95*(1/(80+273.2));
    }, 'Data out of range. Please check the units on the data entered.');


$( ".qc_Q7-5 input" ).each(function(){ 
           $(this).rules('add', {
          required: true,
          rangecheckQ75: true,
         
  });
});

//Question 8 
jQuery.validator.addMethod('totalcheckQ81', function(value, element) {
        var a = 0;
        if($( ".qc_Q8-2 input" ).val()!=""){
          a = a + parseFloat($( ".qc_Q8-2 input" ).val())*1;
        }
        if($( ".qc_Q8-3 input" ).val()!=""){
          a = a + parseFloat($( ".qc_Q8-3 input" ).val())*1;
        }
        if($( ".qc_Q8-4 input" ).val()!=""){
          a = a + parseFloat($( ".qc_Q8-4 input" ).val())*1;
        }
        if($( ".qc_Q8-5 input" ).val()!=""){
          a = a + parseFloat($( ".qc_Q8-5 input" ).val())*1;
        }


        return parseFloat(value) <= 5400 - a;
    }, 'Your entries in this column exceed 5400.');


$( ".qc_Q8-1 input" ).each(function(){
           $(this).rules('add', {
          required: true,
//          number: true,
          totalcheckQ81: true,
  });
});

jQuery.validator.addMethod('totalcheckQ82', function(value, element) {
        var a = 0;
        if($( ".qc_Q8-1 input" ).val()!=""){
          a = a + parseFloat($( ".qc_Q8-1 input" ).val())*1;
        }
        if($( ".qc_Q8-3 input" ).val()!=""){
          a = a + parseFloat($( ".qc_Q8-3 input" ).val())*1;
        }
        if($( ".qc_Q8-4 input" ).val()!=""){
          a = a + parseFloat($( ".qc_Q8-4 input" ).val())*1;
        }
        if($( ".qc_Q8-5 input" ).val()!=""){
          a = a + parseFloat($( ".qc_Q8-5 input" ).val())*1;
        }


        return parseFloat(value) <= 5400 - a;
    }, 'Your entries in this column exceed 5400.');


jQuery.validator.addMethod('descending82', function(value, element) {
    return parseFloat(value) < parseFloat($(".qc_Q8-1 input").val())  
    }, 'You should be entering in descending order');
  
$( ".qc_Q8-2 input" ).each(function(){
           $(this).rules('add', {
          required: true,
//          number: true,
          totalcheckQ82: true,
          descending82: true,
  });
});

jQuery.validator.addMethod('totalcheckQ83', function(value, element) {
        var a = 0;
        if($( ".qc_Q8-1 input" ).val()!=""){
          a = a + parseFloat($( ".qc_Q8-1 input" ).val())*1;
        }
        if($( ".qc_Q8-2 input" ).val()!=""){
          a = a + parseFloat($( ".qc_Q8-2 input" ).val())*1;
        }
        if($( ".qc_Q8-4 input" ).val()!=""){
          a = a + parseFloat($( ".qc_Q8-4 input" ).val())*1;
        }
        if($( ".qc_Q8-5 input" ).val()!=""){
          a = a + parseFloat($( ".qc_Q8-5 input" ).val())*1;
        }
        return parseFloat(value) <= 5400 - a;
    }, 'Your entries in this column exceed 5400.');

jQuery.validator.addMethod('descending83', function(value, element) {
        return parseFloat(value) < parseFloat($(".qc_Q8-2 input").val())  
    }, 'You should be entering in descending order');
  
$( ".qc_Q8-3 input" ).each(function(){
           $(this).rules('add', {
          required: true,
//          number: true,
          totalcheckQ83: true,
          descending83: true,
  });
});

jQuery.validator.addMethod('totalcheckQ84', function(value, element) {
        var a = 0;
        if($( ".qc_Q8-1 input" ).val()!=""){
          a = a + parseFloat($( ".qc_Q8-1 input" ).val())*1;
        }
        if($( ".qc_Q8-2 input" ).val()!=""){
          a = a + parseFloat($( ".qc_Q8-2 input" ).val())*1;
        }
        if($( ".qc_Q8-3 input" ).val()!=""){
          a = a + parseFloat($( ".qc_Q8-3 input" ).val())*1;
        }
        if($( ".qc_Q8-5 input" ).val()!=""){
          a = a + parseFloat($( ".qc_Q8-5 input" ).val())*1;
        }
        return parseFloat(value) <= 5400 - a;
    }, 'Your entries in this column exceed 5400.');

jQuery.validator.addMethod('descending84', function(value, element) {
        return parseFloat(value) < parseFloat($(".qc_Q8-3 input").val())  
    }, 'You should be entering in descending order');
  
$( ".qc_Q8-4 input" ).each(function(){
           $(this).rules('add', {
          required: true,
 //         number: true,
          totalcheckQ84: true,
          descending84: true,
  });
});

jQuery.validator.addMethod('totalcheckQ85', function(value, element) {
        var a = 0;
        if($( ".qc_Q8-1 input" ).val()!=""){
          a = a + parseFloat($( ".qc_Q8-1 input" ).val())*1;
        }
        if($( ".qc_Q8-2 input" ).val()!=""){
          a = a + parseFloat($( ".qc_Q8-2 input" ).val())*1;
        }
        if($( ".qc_Q8-3 input" ).val()!=""){
          a = a + parseFloat($( ".qc_Q8-3 input" ).val())*1;
        }
        if($( ".qc_Q8-4 input" ).val()!=""){
          a = a + parseFloat($( ".qc_Q8-4 input" ).val())*1;
        }
        return parseFloat(value) <= 5400 - a;
    }, 'Your entries in this column exceed 5400.');

jQuery.validator.addMethod('descending85', function(value, element) {
        return parseFloat(value) < parseFloat($(".qc_Q8-4 input").val())  
    }, 'You should be entering in descending order');
  
$( ".qc_Q8-5 input" ).each(function(){
           $(this).rules('add', {
          required: true,
//          number: true,
          totalcheckQ85: true,
          descending85: true,
  });
});


//Question 9 

jQuery.validator.addMethod('rangecheckQ91', function(value, element) {
        return parseFloat(value) <= 1.05*(Math.log(2)/parseFloat($( ".qc_Q8-1 input" ).val())) && parseFloat(value)  >= .95*(Math.log(2)/parseFloat($( ".qc_Q8-1 input" ).val()));
    }, 'Data out of range. Please check the units on the data entered.');


$( ".qc_Q9-1 input" ).each(function(){ 
           $(this).rules('add', {
          required: true,
          rangecheckQ91: true,
         
  });
});

jQuery.validator.addMethod('rangecheckQ92', function(value, element) {
        return parseFloat(value) <= 1.05*(Math.log(2)/parseFloat($( ".qc_Q8-2 input" ).val())) && parseFloat(value)  >= .95*(Math.log(2)/parseFloat($( ".qc_Q8-2 input" ).val()));
    }, 'Data out of range. Please check the units on the data entered.');


$( ".qc_Q9-2 input" ).each(function(){ 
           $(this).rules('add', {
          required: true,
          rangecheckQ92: true,
         
  });
});

jQuery.validator.addMethod('rangecheckQ93', function(value, element) {
        return parseFloat(value) <= 1.05*(Math.log(2)/parseFloat($( ".qc_Q8-3 input" ).val())) && parseFloat(value)  >= .95*(Math.log(2)/parseFloat($( ".qc_Q8-3 input" ).val()));
    }, 'Data out of range. Please check the units on the data entered.');


$( ".qc_Q9-3 input" ).each(function(){ 
           $(this).rules('add', {
          required: true,
          rangecheckQ93: true,
         
  });
});

jQuery.validator.addMethod('rangecheckQ94', function(value, element) {
        return parseFloat(value) <= 1.05*(Math.log(2)/parseFloat($( ".qc_Q8-4 input" ).val())) && parseFloat(value)  >= .95*(Math.log(2)/parseFloat($( ".qc_Q8-4 input" ).val()));
    }, 'Data out of range. Please check the units on the data entered.');


$( ".qc_Q9-4 input" ).each(function(){ 
           $(this).rules('add', {
          required: true,
          rangecheckQ94: true,
         
  });
});

jQuery.validator.addMethod('rangecheckQ95', function(value, element) {
        return parseFloat(value) <= 1.05*(Math.log(2)/parseFloat($( ".qc_Q8-5 input" ).val())) && parseFloat(value)  >= .95*(Math.log(2)/parseFloat($( ".qc_Q8-5 input" ).val()));
    }, 'Data out of range. Please check the units on the data entered.');


$( ".qc_Q9-5 input" ).each(function(){ 
           $(this).rules('add', {
          required: true,
          rangecheckQ95: true,
         
  });
});

//Question 10

jQuery.validator.addMethod('rangecheckQ101', function(value, element) {
        return parseFloat(value) >= 1.05*(Math.log(parseFloat($( ".qc_Q9-1 input" ).val()))) && parseFloat(value) <= .95*(Math.log(parseFloat($( ".qc_Q9-1 input" ).val())));
//Note: reversed the < and > signs because values are negative
    }, 'Data out of range. Please check the units on the data entered.');


$( ".qc_Q10-1 input" ).each(function(){ 
           $(this).rules('add', {
          required: true,
          rangecheckQ101: true,
         
  });
});

jQuery.validator.addMethod('rangecheckQ102', function(value, element) {
        return parseFloat(value) >= 1.05*(Math.log(parseFloat($( ".qc_Q9-2 input" ).val()))) && parseFloat(value) <= .95*(Math.log(parseFloat($( ".qc_Q9-2 input" ).val())));
//Note: reversed the < and > signs because values are negative
    }, 'Data out of range. Please check the units on the data entered.');


$( ".qc_Q10-2 input" ).each(function(){ 
           $(this).rules('add', {
          required: true,
          rangecheckQ102: true,
         
  });
});

jQuery.validator.addMethod('rangecheckQ103', function(value, element) {
        return parseFloat(value) >= 1.05*(Math.log(parseFloat($( ".qc_Q9-3 input" ).val()))) && parseFloat(value) <= .95*(Math.log(parseFloat($( ".qc_Q9-3 input" ).val())));
//Note: reversed the < and > signs because values are negative
    }, 'Data out of range. Please check the units on the data entered.');


$( ".qc_Q10-3 input" ).each(function(){ 
           $(this).rules('add', {
          required: true,
          rangecheckQ103: true,
         
  });
});

jQuery.validator.addMethod('rangecheckQ104', function(value, element) {
        return parseFloat(value) >= 1.05*(Math.log(parseFloat($( ".qc_Q9-4 input" ).val()))) && parseFloat(value) <= .95*(Math.log(parseFloat($( ".qc_Q9-4 input" ).val())));
//Note: reversed the < and > signs because values are negative
    }, 'Data out of range. Please check the units on the data entered.');


$( ".qc_Q10-4 input" ).each(function(){ 
           $(this).rules('add', {
          required: true,
          rangecheckQ104: true,
         
  });
});

jQuery.validator.addMethod('rangecheckQ105', function(value, element) {
        return parseFloat(value) >= 1.05*(Math.log(parseFloat($( ".qc_Q9-5 input" ).val()))) && parseFloat(value) <= .95*(Math.log(parseFloat($( ".qc_Q9-5 input" ).val())));
//Note: reversed the < and > signs because values are negative
    }, 'Data out of range. Please check the units on the data entered.');


$( ".qc_Q10-5 input" ).each(function(){ 
           $(this).rules('add', {
          required: true,
          rangecheckQ105: true,
         
  });
});

