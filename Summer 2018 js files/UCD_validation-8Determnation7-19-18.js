//REQUIRED CODE FOR ALL JQUERY VALIDATION--------------------------------------------------------------------------------------
$('.mform').validate({
    debug: true,
    submitHandler: function(form) {
         form.submit();
   }
});
$.validator.addMethod(
        "regex",
        function(value, element, regexp) {
            var re = new RegExp(regexp);
            return this.optional(element) || re.test(value);
        },
        "Please check your input."
);
$("#id_submitbutton_cancel").addClass("cancel");
//END REQUIRED CODE FOR ALL JQUERY VALIDATION--------------------------------------------------------------------------------------

//EXAMPLE VALIDATIONS-----------------------------------------------------------
// For these examples, Q1 is the field name.
// In the view html, you put the field in a div with class name =qc_Q1: <div class="qc_Q1">[[Q1]]</div>

//RANGE 

	
$( ".qc_Q4 input" ).each(function(){
           $(this).rules('add', {
          range: [50, 200],
          required: true,
          messages: {
               range: "Data out of range. Please check the units on the data entered."
          }
  });
});

$( ".qc_Q6T1 input" ).each(function(){
           $(this).rules('add', {
          range: [0, 75],
          required: true,
          messages: {
               range: "Data out of range. Please check the units on the data entered."
          }
  });
});

$( ".qc_Q6T2 input" ).each(function(){
           $(this).rules('add', {
          range: [0, 75],
          required: true,
          messages: {
               range: "Data out of range. Please check the units on the data entered."
          }
  });
});


$( ".qc_Q6T3 input" ).each(function(){
           $(this).rules('add', {
          range: [0, 75],
          required: true,
          messages: {
               range: "Data out of range. Please check the units on the data entered."
          }
  });
});


$( ".qc_Q10 input" ).each(function(){
           $(this).rules('add', {
          range: [50, 200],
          required: true,
          messages: {
               range: "Data out of range. Please check the units on the data entered."
          }
  });
});

$( ".qc_Q11T1 input" ).each(function(){
           $(this).rules('add', {
          range: [0, 50],
          required: true,
          messages: {
               range: "Data out of range. Please check the units on the data entered."
          }
  });
});

$( ".qc_Q11T2 input" ).each(function(){
           $(this).rules('add', {
          range: [0, 50],
          required: true,
          messages: {
               range: "Data out of range. Please check the units on the data entered."
          }
  });
});

$( ".qc_Q11T3 input" ).each(function(){
           $(this).rules('add', {
          range: [0, 50],
          required: true,
          messages: {
               range: "Data out of range. Please check the units on the data entered."
          }
  });
});

$( ".qc_Q17T1 input" ).each(function(){
           $(this).rules('add', {
          range: [0, 75],
          messages: {
               range: "Data out of range. Please check the units on the data entered."
          }
  });
});

$( ".qc_Q17T2 input" ).each(function(){
           $(this).rules('add', {
          range: [0, 75],
          messages: {
               range: "Data out of range. Please check the units on the data entered."
          }
  });
});

$( ".qc_Q17T3 input" ).each(function(){
           $(this).rules('add', {
          range: [0, 75],
          messages: {
               range: "Data out of range. Please check the units on the data entered."
          }
  });
});

$( ".qc_Q19T1 input" ).each(function(){
           $(this).rules('add', {
          range: [0, 75],
          messages: {
               range: "Data out of range. Please check the units on the data entered."
          }
  });
});

$( ".qc_Q19T2 input" ).each(function(){
           $(this).rules('add', {
          range: [0, 75],
          messages: {
               range: "Data out of range. Please check the units on the data entered."
          }
  });
});

$( ".qc_Q19T3 input" ).each(function(){
           $(this).rules('add', {
          range: [0, 75],
          messages: {
               range: "Data out of range. Please check the units on the data entered."
          }
  });
});

jQuery.validator.addMethod('rangecheckQ71', function(value, element) {
        return parseFloat(value) <= 10*parseFloat($(".qc_Q5").val()) ;
    }, 'ALERT');


$( "input[class='qc_Q5']" ).each(function(){
           $(this).rules('add', {
          required: true,
          number: true,
          messages: {
               range: "This field must be a number"
          }
  });
});

$( "input[class^='qc_Q7T1']" ).each(function(){
           $(this).rules('add', {
          required: true,
          rangecheckQ71: true,
         
  });
});


jQuery.validator.addMethod('dobleValor', function(value, element) {
        return parseFloat(value) <= 2*parseFloat($(".validation_x_value").val()) && parseFloat(value) >= 0;
    }, 'ALERT');


$( "input[class='validation_x_value']" ).each(function(){
           $(this).rules('add', {
          required: true,
          number: true,
          messages: {
               range: "This field must be a number"
          }
  });
});

$( "input[class^='validation_only_range_function(between_0_and_no_more_than_twice_the_value_of_x)']" ).each(function(){
           $(this).rules('add', {
          required: true,
          dobleValor: true,
         
  });
});




