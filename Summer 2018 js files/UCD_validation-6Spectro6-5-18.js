//REQUIRED CODE FOR ALL JQUERY VALIDATION--------------------------------------------------------------------------------------
$('.mform').validate({
    debug: true,
    submitHandler: function(form) {
         form.submit();
   }
});
$.validator.addMethod(
        "regex",
        function(value, element, regexp) {
            var re = new RegExp(regexp);
            return this.optional(element) || re.test(value);
        },
        "Please check your input."
);
$("#id_submitbutton_cancel").addClass("cancel");
//END REQUIRED CODE FOR ALL JQUERY VALIDATION--------------------------------------------------------------------------------------

//EXAMPLE VALIDATIONS-----------------------------------------------------------
// For these examples, Q1 is the field name.
// In the view html, you put the field in a div with class name =qc_Q1: <div class="qc_Q1">[[Q1]]</div>

	
	
$( ".qc_Q4 input" ).each(function(){
           $(this).rules('add', {
          range: [50, 300],
          required: true,
          messages: {
               range: "Data out of range. Please check the units on the data entered."
          }
  });
});

$( ".qc_Q5 input" ).each(function(){
           $(this).rules('add', {
          range: [20, 30],
          required: true,
          messages: {
               range: "Data out of range. Please check the units on the data entered."
          }
  });
});

$( ".qc_Q7 input" ).each(function(){
           $(this).rules('add', {
          range: [570, 670],
          required: true,
          messages: {
               range: "Data out of range. Please check the units on the data entered."
          }
  });
});

$( ".qc_Q8 input" ).each(function(){
           $(this).rules('add', {
          range: [450, 550],
          required: true,
          messages: {
               range: "Data out of range. Please check the units on the data entered."
          }
  });
});

$( ".qc_Q10 input" ).each(function(){
           $(this).rules('add', {
          range: [0, 1],
          required: true,
          messages: {
               range: "Data out of range. Please check the units on the data entered."
          }
  });
});

$( ".qc_Q11 input" ).each(function(){
           $(this).rules('add', {
          range: [0, 1],
          required: true,
          messages: {
               range: "Data out of range. Please check the units on the data entered."
          }
  });
});

$( ".qc_Q12 input" ).each(function(){
           $(this).rules('add', {
          range: [0, 1],
          required: true,
          messages: {
               range: "Data out of range. Please check the units on the data entered."
          }
  });
});

$( ".qc_Q13 input" ).each(function(){
           $(this).rules('add', {
          range: [0, 1],
          required: true,
          messages: {
               range: "Data out of range. Please check the units on the data entered."
          }
  });
});


$( ".qc_Q15 input" ).each(function(){
           $(this).rules('add', {
          range: [0, 1],
          required: true,
          messages: {
               range: "Data out of range. Please check the units on the data entered."
          }
  });
});

$( ".qc_Q16 input" ).each(function(){
           $(this).rules('add', {
          range: [0, 1],
          required: true,
          messages: {
               range: "Data out of range. Please check the units on the data entered."
          }
  });
});




