//REQUIRED CODE FOR ALL JQUERY VALIDATION--------------------------------------------------------------------------------------
$('.mform').validate({
    debug: true,
    submitHandler: function(form) {
         form.submit();
   }
});
$.validator.addMethod(
        "regex",
        function(value, element, regexp) {
            var re = new RegExp(regexp);
            return this.optional(element) || re.test(value);
        },
        "Please check your input."
);
$("#id_submitbutton_cancel").addClass("cancel");
//END REQUIRED CODE FOR ALL JQUERY VALIDATION--------------------------------------------------------------------------------------

//Descending Order-------------------------------------------------
//Q9-----------


jQuery.validator.addMethod('descending92', function(value, element) {
    return parseFloat(value) < parseFloat($(".qc_Q9-1 input").val())  
    }, 'You should be entering in descending order');
	
$( ".qc_Q9-2 input" ).each(function(){
           $(this).rules('add', {
          required: true,
          descending92: true,
         
  });
});

jQuery.validator.addMethod('descending93', function(value, element) {
        return parseFloat(value) < parseFloat($(".qc_Q9-2 input").val());
    }, 'You should be entering in descending order');
	
$( ".qc_Q9-3 input" ).each(function(){
           $(this).rules('add', {
          required: true,
          descending93: true,
         
  });
});

jQuery.validator.addMethod('descending94', function(value, element) {
        return parseFloat(value) < parseFloat($(".qc_Q9-3 input").val());
    }, 'You should be entering in descending order');
	
$( ".qc_Q9-4 input" ).each(function(){
           $(this).rules('add', {
          required: true,
          descending94: true,
         
  });
});

//Q10-----------

jQuery.validator.addMethod('descending102', function(value, element) {
        return parseFloat(value) < parseFloat($(".qc_Q10-1 input").val())  
    }, 'You should be entering in descending order');
	
$( ".qc_Q10-2 input" ).each(function(){
           $(this).rules('add', {
          required: true,
          descending102: true,
         
  });
});

jQuery.validator.addMethod('descending103', function(value, element) {
        return parseFloat(value) < parseFloat($(".qc_Q10-2 input").val());
    }, 'You should be entering in descending order');
	
$( ".qc_Q10-3 input" ).each(function(){
           $(this).rules('add', {
          required: true,
          descending103: true,
         
  });
});

jQuery.validator.addMethod('descending104', function(value, element) {
        return parseFloat(value) < parseFloat($(".qc_Q10-3 input").val());
    }, 'You should be entering in descending order');
	
$( ".qc_Q10-4 input" ).each(function(){
           $(this).rules('add', {
          required: true,
          descending104: true,
         
  });
});
//Q11-----------

jQuery.validator.addMethod('descending112', function(value, element) {
        return parseFloat(value) < parseFloat($(".qc_Q11-1 input").val())  
    }, 'You should be entering in descending order');
	
$( ".qc_Q11-2 input" ).each(function(){
           $(this).rules('add', {
          required: true,
          descending112: true,
         
  });
});

jQuery.validator.addMethod('descending113', function(value, element) {
        return parseFloat(value) < parseFloat($(".qc_Q11-2 input").val());
    }, 'You should be entering in descending order');
	
$( ".qc_Q11-3 input" ).each(function(){
           $(this).rules('add', {
          required: true,
          descending113: true,
         
  });
});

jQuery.validator.addMethod('descending114', function(value, element) {
        return parseFloat(value) < parseFloat($(".qc_Q11-3 input").val());
    }, 'You should be entering in descending order');
	
$( ".qc_Q11-4 input" ).each(function(){
           $(this).rules('add', {
          required: true,
          descending114: true,
         
  });
});
//Q12-----------

jQuery.validator.addMethod('descending122', function(value, element) {
        return parseFloat(value) < parseFloat($(".qc_Q12-1 input").val())  
    }, 'You should be entering in descending order');
	
$( ".qc_Q12-2 input" ).each(function(){
           $(this).rules('add', {
          required: true,
          descending122: true,
         
  });
});

jQuery.validator.addMethod('descending123', function(value, element) {
        return parseFloat(value) < parseFloat($(".qc_Q12-2 input").val());
    }, 'You should be entering in descending order');
	
$( ".qc_Q12-3 input" ).each(function(){
           $(this).rules('add', {
          required: true,
          descending123: true,
         
  });
});

jQuery.validator.addMethod('descending124', function(value, element) {
        return parseFloat(value) < parseFloat($(".qc_Q12-3 input").val());
    }, 'You should be entering in descending order');
	
$( ".qc_Q12-4 input" ).each(function(){
           $(this).rules('add', {
          required: true,
          descending124: true,
         
  });
});

//Q13-----------
jQuery.validator.addMethod('descending132', function(value, element) {
        return parseFloat(value) < parseFloat($(".qc_Q13-1 input").val())  
    }, 'You should be entering in descending order');
	
$( ".qc_Q13-2 input" ).each(function(){
           $(this).rules('add', {
          required: true,
          descending132: true,
         
  });
});

jQuery.validator.addMethod('descending133', function(value, element) {
        return parseFloat(value) < parseFloat($(".qc_Q13-2 input").val());
    }, 'You should be entering in descending order');
	
$( ".qc_Q13-3 input" ).each(function(){
           $(this).rules('add', {
          required: true,
          descending133: true,
         
  });
});

jQuery.validator.addMethod('descending134', function(value, element) {
        return parseFloat(value) < parseFloat($(".qc_Q13-3 input").val());
    }, 'You should be entering in descending order');
	
$( ".qc_Q13-4 input" ).each(function(){
           $(this).rules('add', {
          required: true,
          descending134: true,
         
  });
});

//END Descending Order-------------------------------------------------